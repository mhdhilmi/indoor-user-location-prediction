# Indoor User Location Prediction

Solution of Ericsson Machine Learning 2022
#challenge5 : Indoor User Location Prediction

## Getting started

Make sure you have jupyter notebook installed on your machine. 
Then, you can directly open jupyter file: ML Hackathon_200_Ombilin-Team_Challenge_5.ipynb

If you get confused, please read our final report on ML Hackathon_200_Ombilin-Team_Challenge_5.pptx

```
cd existing_repo
git clone https://gitlab.com/mhdhilmi/indoor-user-location-prediction.git
```

Happy Exploring!